import configparser
import os
#from app import app_env

app_config = configparser.ConfigParser()
app_config.read('application/conf/application.conf')

db_conf = configparser.ConfigParser()
db_conf.read('application/conf/db.conf')

server_port = app_config.get('SERVER', 'port')
server_host = app_config.get('SERVER', 'host')
da_api_base_service_url = "/api/nc/deviation_cluster"
es_api_base_service_url = "/api/nc/executive_summary"

#database_type = app_config.get('SERVER','env')
print(os.environ)
if os.environ["env"][1::] == "dev":
    database = db_conf.get('DATABASE_DEV', 'db_name')
    password = db_conf.get('DATABASE_DEV', 'password')
    port = db_conf.get('DATABASE_DEV', 'port')
    host = db_conf.get('DATABASE_DEV', 'host')
    user = db_conf.get('DATABASE_DEV', 'user')
    cluster = db_conf.get('DATABASE_DEV', 'cluster')

elif os.environ["env"][1::] == "prod":
    database = db_conf.get('DATABASE_PROD', 'db_name')
    password = db_conf.get('DATABASE_PROD','password')
    port = db_conf.get('DATABASE_PROD','port')
    host = db_conf.get('DATABASE_PROD','host')
    user = db_conf.get('DATABASE_PROD','user')
    cluster = db_conf.get('DATABASE_PROD','cluster')
#else:
 #    database = db_conf.get('DATABASE_DEV', 'db_name')
  #   password = db_conf.get('DATABASE_DEV', 'password')
   #  port = db_conf.get('DATABASE_DEV', 'port')
    # host = db_conf.get('DATABASE_DEV', 'host')
  #   user = db_conf.get('DATABASE_DEV', 'user')
   #  cluster = db_conf.get('DATABASE_DEV', 'cluster')

# log_level = app_config.get('LOG', 'log_level')
# log_basepath = app_config.get('LOG', 'base_path')
# log_filename = log_basepath + app_config.get('LOG', 'file_name')
# log_handlers = app_config.get('LOG', 'handlers')
# logger_name = app_config.get('LOG', 'logger_name')

access_key = db_conf.get('S3', 'access_key')
secret_key = db_conf.get('S3', 'secret_key')
region_name = db_conf.get('S3', 'region_name')
