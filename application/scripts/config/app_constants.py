from application.scripts.config import app_configurations
from  datetime import  datetime

class DatabaseTables(object):
    nc_cluster_output = "nc_deviation.nc_cluster_output_long"
    nc_processed_long = "nc_deviation.nc_deviation_processed_long"
    es_trending_line = "nc_deviation.nc_deviation_trend_line_graph"
    es_resolution_status = "nc_deviation.nc_deviation_resolution_status_pie_chart"
    # cluster_output_long_short = "nc.deviation.cluster_output_long_short"

class DeviationAnalysis(object):
    api_list_dropdown = app_configurations.da_api_base_service_url + "/list-dropdown"
    api_list_clusters = app_configurations.da_api_base_service_url + "/list-clusters"
    api_cluster_keyphrases = app_configurations.da_api_base_service_url + "/get-cluster-freq-terms"
    api_cluster_functional_area = app_configurations.da_api_base_service_url + "/get-cluster-functional-area"
    api_cluster_row_data = app_configurations.da_api_base_service_url + "/get-cluster-row-data"
    api_download_row_data = app_configurations.da_api_base_service_url + "/download-data"
    api_get_refresh_date = app_configurations.da_api_base_service_url + "/get-refresh-date"
    KEY_RESULT = "result"

class ExecutiveSummary(object):
    api_list_years = app_configurations.es_api_base_service_url + "/list-years"
    api_montly_dev_trend = app_configurations.es_api_base_service_url + "/monthly-deviation-trend"
    api_resolution_status = app_configurations.es_api_base_service_url + "/deviation-resolution-status"
    api_total_deviation = app_configurations.es_api_base_service_url + "/get-total-deviation"
    KEY_RESULT = "result"

def result_template(data):
    return {
        "status": "Success", "timestamp": datetime.now(), "response" :
    data
    }

# class DDL:
#     create_cluster_output_table_ddl = f"""CREATE TABLE IF NOT EXISTS "{DatabaseTables.cluster_output_long_short}"(
#                      "sr_no" varchar(20),
#                      "year" int,
#                      "resolution_status" varchar(25),
#                      "function" varchar(25),
#                      "plant_code_observed" varchar(25),
#                      "severity" varchar(25),
#                      "cluster_name" varchar(25),
#                      "key_phrases" varchar);"""

#     insert_cluster_output_table_ddl = f""" INSERT INTO "{DatabaseTables.cluster_output_long_short}" (sr_no, year, resolution_status, function, plant_code_observed,
#                                        severity, cluster_name, key_phrases) SELECT sr_no, DATEPART(year, observation_date), resolution_status, function, plant_code_observed, severity, cluster_name,
#                                         key_phrases FROM "{DatabaseTables.cluster_output}"; """
