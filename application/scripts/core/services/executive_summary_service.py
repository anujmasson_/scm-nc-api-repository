from fastapi import APIRouter, HTTPException, Query
from application.scripts.config import app_constants
# from application.scripts.logging.app_loggers import logger
from application.scripts.core.handlers.executive_summary_handler import ExecutiveSummary
from typing import Optional, List
router = APIRouter()

@router.get(app_constants.ExecutiveSummary.api_list_years)
def list_year():
    try:
        obj = ExecutiveSummary()
        result = obj.list_years()
        return result
    except Exception as e:
        # logger.exception('Exception in Fetch Years Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))


@router.get(app_constants.ExecutiveSummary.api_montly_dev_trend)
def monthly_dev_trend(year: Optional[int] = None, comparison_year: Optional[int]= None):
    try:
        obj = ExecutiveSummary()
        result = obj.monthly_deviation_trend(year, comparison_year)
        return result

    except Exception as e:
        # logger.exception('Exception in Monthly Deviation Trend Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.ExecutiveSummary.api_resolution_status)
def deviation_resolution_status(year: Optional[int] = None):
    try:
        obj = ExecutiveSummary()
        result = obj.deviation_resolution_status(year)
        return result

    except Exception as e:
        # logger.exception('Exception in Deviation Resolution Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.ExecutiveSummary.api_total_deviation)
def total_deviation(view_by:str, stack_by:str, year: Optional[str] = None, filter_val: Optional[str] = None, filter_by: Optional[str] = None, include_resolved:  Optional[bool] = False):
    try:
        obj = ExecutiveSummary()
        result = obj.total_deviation(view_by, stack_by,filter_val, filter_by, include_resolved,  year)
        return result
    except Exception as e:
        raise HTTPException(status_code = 500, detail = str(e))
