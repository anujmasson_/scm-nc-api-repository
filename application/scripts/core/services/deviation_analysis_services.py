from fastapi import APIRouter, HTTPException, Query
from application.scripts.config import app_constants
# from application.scripts.logging.app_loggers import logger
from application.scripts.core.handlers.deviation_analysis_handler import DeviationAnalysisHandler
from typing import Optional, List
router = APIRouter()

@router.get(app_constants.DeviationAnalysis.api_list_dropdown)
def list_dropdown():
    try:
        obj = DeviationAnalysisHandler()
        result = obj.list_dropdown()
        return result

    except Exception as e:
        # logger.exception('Exception in List Dropdown Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_list_clusters)
def get_clusters(severity: Optional[str] = None, resolution_status: List[str] = Query(None), year: List[str] = Query(None), plant: List[str] = Query(None)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_clusters(year, severity, resolution_status, plant)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_cluster_keyphrases)
def get_cluster_graph( severity: Optional[str] = None , cluster_name:Optional[str] = None, resolution_status: List[str] = Query(None), plant: List[str] = Query(None), year: List[str] = Query(None)):

    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_cluster_keyphrases(year, severity, resolution_status, cluster_name, plant)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Frequent Terms Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_cluster_functional_area)
def get_functional_area( severity: Optional[str] = None , cluster_name:Optional[str] = None, resolution_status: List[str] = Query(None), plant: List[str] = Query(None), year: List[str] = Query(None)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_cluster_functional_area(year, severity, resolution_status, cluster_name, plant)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Functional Area Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_cluster_row_data)
def get_cluster_row_data(limit:int, offset: int, severity: Optional[str] = None , cluster_name:Optional[str] = None, resolution_status: List[str] = Query(None), plant: List[str] = Query(None), year: List[str] = Query(None)):
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_cluster_row_data(limit, offset, year, severity, resolution_status, cluster_name, plant)
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Functional Area Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.post(app_constants.DeviationAnalysis.api_download_row_data)
def get_cluster_row_data():
    try:
        obj = DeviationAnalysisHandler()
        result = obj.download_row_data()
        return result
    except Exception as e:
        # logger.exception('Exception in Get Cluster Functional Area Service' + str(e))
        raise HTTPException(status_code=500, detail=str(e))

@router.get(app_constants.DeviationAnalysis.api_get_refresh_date)
def get_refresh_date():
    try:
        obj = DeviationAnalysisHandler()
        result = obj.get_refresh_date()
        return result
    except Exception as e:
        raise HTTPException(status_code = 500, detail = str(e))
