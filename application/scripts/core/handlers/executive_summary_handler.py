from fastapi import HTTPException
from application.scripts.config import app_constants
# from application.scripts.logging.app_loggers import logger
from application.scripts.utilities.redshift_utility import RedShiftUtility
from application.scripts.utilities.s3_utility import S3Utility
import traceback

database_object = RedShiftUtility()
s3_object = S3Utility()

class ExecutiveSummary(object):
    def __init__(self):
        """

        """

    def list_years(self):
        try:
            year = f""" select distinct year from {app_constants.DatabaseTables.es_trending_line} order by year ASC """
            year_query = database_object.execute_select_query(year)
            year_data = [item for t in year_query for item in t]
            result = {
                "year": year_data
            }
            return app_constants.result_template(result)


        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occuerd while fetching no of clusters" + str(e))
            raise HTTPException(status_code=500, detail= str(e))

    def monthly_deviation_trend(self, year, comparison_year):
        try:
            details = []

            if year is None and comparison_year is None:
                average_monthly_trend_query = f""" select month, avg(total_deviation_count) from {app_constants.DatabaseTables.es_trending_line} group by month """
                average_monthly_data = database_object.execute_select_query(average_monthly_trend_query)
                monthly_deviation = []  
                year = "All"
                
                for i in average_monthly_data:
                    monthly_deviation.append({"month":i[0],"count":i[1]})
                result = {"year":year,"monthly_deviation":monthly_deviation}   
                
            else:
                year = f""" select distinct month, total_deviation_count from {app_constants.DatabaseTables.es_trending_line} where year = {year}"""
                year_query = database_object.execute_select_query(year)
                comparison_year = f""" select distinct month, total_deviation_count from {app_constants.DatabaseTables.es_trending_line} where year = {comparison_year} """
                comparison_year_query = database_object.execute_select_query(comparison_year)
                year_data = [item for t in year_query for item in t]
                comparison_data = [item for t in comparison_year_query for item in t]
                monthly_deviation = []
                comparison_year_list = []
                for i in year_query:
                    monthly_deviation.append({"month" :i[0], "count":i[1]})
                for j in comparison_year_query:
                    comparison_year_list.append({"month":j[0], "count":j[1]})
                for year  in year_data:
                    details.append({"year": year,
                        "monthly_deviation":monthly_deviation})
                    for each_comp_year in comparison_data:
                        details.append({"year":each_comp_year, "monthly_deviation":comparison_year_list})
                result = {"details":details}
            return app_constants.result_template(result)
        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occuerd while fetching no of clusters" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def deviation_resolution_status(self, year):
        try:
            if year is None:
                average_res_status_query = f""" select resolution_status, avg(total_deviation_count) from {app_constants.DatabaseTables.es_resolution_status} group by resolution_status  """
                average_res_status_all_year = database_object.execute_select_query(average_res_status_query)
                details = []
                for i in average_res_status_all_year:
                    details.append({"status":i[0], "count":i[1]})
                result = {"details":details}

            else:            
                yearly_res_status_query = f""" select resolution_status, total_deviation_count, round(100.0 * total_deviation_count/ sum(total_deviation_count) over(), 2) from {app_constants.DatabaseTables.es_resolution_status} where observation_year = {year} group by resolution_status, total_deviation_count """
                res_status = database_object.execute_select_query(yearly_res_status_query)
                details = []
                for i in res_status:
                    details.append({"status":i[0], "count":i[1], "percentage":i[2]})
                result = {"details":details}

            return app_constants.result_template(result)
        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occuerd while fetching no of clusters" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def total_deviation(self, view_by, stack_by, filter_val, filter_by,  include_resolved, year):
        try:
            view_list = ["Plant", "FunctionalArea"]
            show = ["ResolutionStatus", "Severity"]
            query = ""
            
            database_map = {
                    "Plant":"plant_code_observed",
                    "FunctionalArea":"functional_area"
                    }
            stack_column_names = {
                    "Severity":"severity",
                    "ResolutionStatus":"resolution_status"
                    }
            if view_by in view_list and stack_by in show:
                current_view = database_map[view_by]
                current_stack = stack_column_names[stack_by]
                total_deviation_query = f""" select distinct {current_view}, {current_stack}, count({current_view}) from {app_constants.DatabaseTables.nc_processed_long} where DATEPART(year, observation_date) = {year} group by {current_view}, {current_stack} order by {current_view}  """
                total_deviation_data = database_object.execute_select_query(total_deviation_query)
                temp_dict = dict()
                for each_record in total_deviation_data:
                    temp_dict[each_record[0]] = []
                for data in temp_dict:
                    for each_record in total_deviation_data:
                        if each_record[0] == data:
                            temp_dict[each_record[0]].append({
                                "key":each_record[1],
                                "count":each_record[2]
                                })
                #current_view_key = [key for key, value in database_map.items()]
                count=temp_dict
                response = {"view_by":view_by, "stack_by":stack_by,"count":count}
            if view_by in view_list and stack_by in show and filter_by in view_list:
                current_view = database_map[view_by]
                current_stack = stack_column_names[stack_by]
                filter_stack = database_map[view_by]
                print(current_view, current_stack, filter_stack)
                total_dev_filter_by_query = f""" select distinct {current_view}, {current_stack}, count({current_view}) from {app_constants.DatabaseTables.nc_processed_long} where {filter_stack} = {filter_val} and DATEPART(year, observation_date) = {year} group by {current_view} {current_stack} order by {current_view}   """
                total_dev_filter_by = database_object.execute_select_query(total_dev_filter_by_query)
                print(total_dev_filter_by)
                temp_dict = dict()
                for each_record in total_dev_filter_by:
                    temp_dict[each_record[0]] = []
                for data in temp_dict:
                    for each_record in total_dev_filter_by:
                        if each_record[0] == data:
                            temp_dict[each_record[0]].append({
                                "key":each_record[1],
                                "count":each_record[2]
                                })
                print(temp_dict)
             # count.append({"view_by_val":filter_val, "total_count":1})
            # response = {"view_by":view_by, "stack_by":stack_by, "count":count}
            return app_constants.result_template(response)

        except exception as e:
            traceback.print_exc()
            raise HTTPException(status_code = 500, detail = str(e))
