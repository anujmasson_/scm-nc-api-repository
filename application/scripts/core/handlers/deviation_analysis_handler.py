from fastapi import HTTPException
from application.scripts.config import app_constants
from application.scripts.config import app_configurations
# from application.scripts.logging.app_loggers import logger
from application.scripts.utilities.redshift_utility import RedShiftUtility
from application.scripts.utilities.s3_utility import S3Utility
import traceback
import sys
import os
import boto3
import s3fs
import sagemaker
import pandas as pd
from io import StringIO

database_object = RedShiftUtility()
s3_object = S3Utility()

class DeviationAnalysisHandler(object):
    def __init__(self):
        """

        """

    def list_dropdown(self):
        try:
            """
            Function to fetch all dropdown values by default
            :return dropdown
            """

            year = f""" select distinct year from {app_constants.DatabaseTables.nc_cluster_output} order by year ASC """ 
            severity = f""" select distinct severity from {app_constants.DatabaseTables.nc_cluster_output} """
            res_status = f""" select distinct resolution_status from {app_constants.DatabaseTables.nc_cluster_output} """
            plant_code = f""" select distinct plant_code_observed from {app_constants.DatabaseTables.nc_cluster_output} """
            year_query = database_object.execute_select_query(year)
            severity_query = database_object.execute_select_query(severity)
            res_status_query = database_object.execute_select_query(res_status)
            plant_code_query = database_object.execute_select_query(plant_code)
            year_data = [item for t in year_query for item in t]
            severity_data = [item for t in severity_query for item in t]
            res_status_data = [item for t in res_status_query for item in t]
            plant_code_data = [item for t in plant_code_query for item in t]

            result = {"year": year_data,
                      "severity": severity_data,
                      "resolution_status": res_status_data,
                      "plant":plant_code_data
                  }

            return app_constants.result_template(result)

        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching dropdown options" + str(e))
            raise HTTPException(status_code=500, detail=str(e))

    def get_top_3_keyphrases(self,term_list):
        top_3_list = []
        from collections import Counter
        try:
            top_3_keyphrases = [terms[0] for terms in Counter(term_list).most_common(3)]
            return top_3_keyphrases
        except Exception as e:
            raise HTTPException (status_code = 500, detail = str(e))



    def get_clusters(self, year, severity, resolution_status, plant_code):
        try:
            """
            Function to fetch all clusters based on query params
            :return no of clusters 
            """
           
            year_condition = ""
            res_status_condition = ""
            plant_code_condition = ""

            if year!=None:
                year = ','.join(map(str,year))
                year_condition += f""" and year in ({year}) """
            if resolution_status != None:
                res_status_condition = ','.join(map(str,resolution_status))
                res_status_condition += f""" and resolution_status in ('{resolution_status}')  """
            if plant_code!= None:
                plant_code = ','.join(map(str,plant_code))
                plant_code_condition += f""" and plant_code in ('{plant_code}')  """

            cluster_info = []
            temp_dict = dict() 
            list_cluster_query = f""" select cluster_name,  x, y, keyphrases from {app_constants.DatabaseTables.nc_cluster_output}
                    where  severity = '{severity}'  {year_condition}  {res_status_condition}  {plant_code_condition}  """
            cluster_records = database_object.execute_select_query(list_cluster_query)
            
            for each_details in cluster_records:
                #if each_details[0] == "C18":
                    #print(each_details)
                if each_details[0] not in temp_dict:
                    template_dict = {"cluster_name" : each_details[0],"total_deviations" : 1, "X": round(each_details[1],2), "Y": round(each_details[2], 2), "each_terms" : (each_details[3]).strip("][").replace("'","").split(", ")}

                    temp_dict[each_details[0]] = template_dict
                else:

                    temp_dict[each_details[0]]["X"] = round(temp_dict[each_details[0]]["X"] + each_details[1], 2)
                    temp_dict[each_details[0]]["Y"] = round(temp_dict[each_details[0]]["Y"] + each_details[2], 2)
                    temp_dict[each_details[0]]["total_deviations"] = temp_dict[each_details[0]]["total_deviations"] + 1
                    extended_list = (each_details[3]).strip("][").replace("'", "").split(", ")
                    temp_dict[each_details[0]]["each_terms"].extend(extended_list)

            cluster_data = [each_values for each_values in temp_dict.values()]
            number_of_clusters = 0
            cluster_distribution = 0
            for each_list in cluster_data:
                cluster_distribution = cluster_data.index(each_list) + 1
                each_list["top_3_keyphrases"] = self.get_top_3_keyphrases(each_list["each_terms"])
                del each_list["each_terms"]
                number_of_clusters = number_of_clusters + int(each_list['total_deviations'])          

            temp_value = {
                   "number_of_clusters":cluster_distribution,
                   "cluster_distribution":number_of_clusters,
                   "cluster_info": cluster_data
                   }
            
            return app_constants.result_template(temp_value)
        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occuerd while fetching no of clusters" + str(e))
            raise HTTPException(status_code=500, detail=str(e))

    def get_cluster_keyphrases(self, year, severity, resolution_status, cluster_name, plant):
        try:
            """
            Function to fetch all frequent terms
            :return no of clusters with frequent terms
            """

            # for y in top_10_freq:
            #     list2 = y[0].strip('"[],')
            #     w1 = list2.replace("'", "")
            #     z1 = list(w1.split(","))
            #     new_list1.append(z1)
            # data_result = []
            # for index, val in enumerate(new_list):
            #     new_dict = dict(zip(new_list[index], new_list1[index]))
            #     data_result.append(new_dict)
            # l = {}
            # for item in data_result:
            #     for key in item.keys():
            #         if key not in l:
            #             b = {key: item[key]}
            #             l.update(b)
            #         else:
            #             l[key] = str(int(l[key]) + int(item[key]))

            # sorted_freq_terms = sorted(l.items(), key = lambda x:x[1])
            # print(sorted_freq_terms)
            
            from collections import Iterable
            from collections import Counter

            year_condition = ""
            res_status_condition =""
            plant_code_condition = ""
            if year!=None:
                year = ','.join(map(str, year))
                year_condition = year_condition + f"""and year in ({year})"""

            if resolution_status!=None:
                resolution_status = ','.join(map(str,resolution_status))
                res_status_condition = res_status_condition +  f""" and resolution_status in ('{resolution_status}') """
            # else:
            #     res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if plant!= None:
                plant = ','.join(map(str, plant))
                plant_code_condition = plant_code_condition  + f""" and plant_code_observed in ('{plant}') """
            # else:
            #     plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} """
            if cluster_name is None:
                top_10_keyphrases_query = f""" select keyphrases from {app_constants.DatabaseTables.nc_cluster_output} where severity = '{severity}' {year_condition} {res_status_condition} {plant_code_condition}  """
                top_10_keyphrases = database_object.execute_select_query(top_10_keyphrases_query)
          
            #from collections import Iterable
            #from collections import Counter
                top_10_keyphrase_list = []
                for each_record in top_10_keyphrases:
                    top_10_phrases = each_record[0].strip('"[],')
                    phrases = top_10_phrases.replace("'","")
                    split_phrases = list(phrases.split(","))
                    top_10_keyphrase_list.append(split_phrases)
                                 
             
                count_keyphrases = Counter(records for sublist in top_10_keyphrase_list for records in sublist)
                top_10_final_keyphrases = count_keyphrases.most_common(10)
            
                data = []
                category = []
                for index, value in enumerate(top_10_final_keyphrases):
                    category.append(value[0])
                    data.append(value[1])
               
                top_10_all_keyphrases = dict(top_10_final_keyphrases)
            
            else:
                top_10_keyphrases_cluster_query = f""" select keyphrases from {app_constants.DatabaseTables.nc_cluster_output}  where cluster_name = '{cluster_name}' and severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition}"""
                top_10_keyphrase_cluster_data = database_object.execute_select_query(top_10_keyphrases_cluster_query)
                top_10_keyphrase_list = []
                for each_record  in top_10_keyphrase_cluster_data:
                    top_10_cluster_keyphrase = each_record[0].strip('"[],')
                    cluster_keyphrase = top_10_cluster_keyphrase.replace("'", "")
                    split_cluster_keyphrase = list(cluster_keyphrase.split(","))
                    top_10_keyphrase_list.append(split_cluster_keyphrase)

                data = Counter(records for sublist in top_10_keyphrase_list for records in sublist)
                top_10_cluster_keyphrases = data.most_common(10)
            
                data = []
                category = []
                for index, value in enumerate(top_10_cluster_keyphrases):
                    category.append(value[0])
                    data.append(value[1])
                top_10_specific_cluster_keyphrases = dict(top_10_cluster_keyphrases)
            result = {
                    "category":category,
                    "data": data                
                  }

            return app_constants.result_template(result)

        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching cluster freq terms" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def get_cluster_functional_area(self, year, severity, resolution_status, cluster_name, plant):
        try:
            """
            Function to fetch all functional area  by default and based on year, severity, res_status, plant_code
            :return no of clusters with functional area
            """
        
            year_condition = ""
            res_status_condition = ""
            plant_code_condition = ""
         
            if year != None:
                year = ','.join(map(str, year))
                year_condition = year_condition + f"""and DATEPART(year, observation_date) in ({year})"""
                print(year_condition)
            
            if resolution_status != None:
                resolution_status = ','.join(map(str, resolution_status))
                res_status_condition = res_status_condition + f""" and resolution_status in ('{resolution_status}') """
            # else:
            #     res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if plant != None:
                plant_code = ','.join(map(str, plant))
                plant_code_condition = plant_code_condition  + f""" and plant_code_observed in ('{plant}') """
            # else:
            #     plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} ""
            if cluster_name is None:
                functional_area_query = f""" select functional_area, count(functional_area) from {app_constants.DatabaseTables.nc_processed_long} where severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition}  group by functional_area """
                functional_area_data = database_object.execute_select_query(functional_area_query)
                category = []
                details = []
                for i, j in enumerate(functional_area_data):
                    category.append(j[0])
                    details.append(j[1])

                functional_area_list_data = [item for t in functional_area_data for item in t]
                functional_area_dict_data = {functional_area_list_data[i] : functional_area_list_data[i+1] for i in range(0, len(functional_area_list_data),2)}
           
            else:
                cluster_functional_data_query = f""" select functional_area, count(functional_area) from {app_constants.DatabaseTables.nc_processed_long}  where cluster_name = '{cluster_name}' and severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition} group by functional_area """
                cluster_functional_area = database_object.execute_select_query(cluster_functional_data_query)
                category = []
                details = []
                for i ,j  in enumerate(cluster_functional_area): 
                    category.append(j[0])
                    details.append(j[1])
                
                
           # cluster_functional_area_list_data = [item for t in cluster_functional_area for item in t]
            #cluster_functional_area_dict_data = {functional_area_list_data[i]: functional_area_list_data[i + 1] for i in
             #            range(0, len(cluster_functional_area_list_data), 2)}
            
            
            result = {
                   "category":category,
                   "details":details

              #  "functional_area_all_cluster": functional_area_dict_data,
               # "functional_area_specific_cluster": cluster_functional_area_dict_data

            }
            return app_constants.result_template(result)


        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching cluster freq terms" + str(e))
            raise HTTPException(status_code=500, detail=str(e))

    def get_cluster_row_data(self,limit, offset, year, severity, resolution_status, cluster_name, plant):
        try:
            """
            Function to fetch entire cluster data based on cluster_name, year, severity, res_status, plant_code
            :return cluster details
            """
            year_condition = ""
            res_status_condition = ""
            plant_code_condition = ""
            if year != None:
                year = ','.join(map(str, year))
                year_condition = year_condition + f"""and DATEPART(year,observation_date) in ({year})"""

            if resolution_status != None:
                resolution_status = ','.join(map(str, resolution_status))
                res_status_condition = res_status_condition + f""" and resolution_status in ('{resolution_status}') """
            # else:
            #     res_status_condition = f""" resolution_status in {tuple(resolution_status)} """

            if plant != None:
                plant = ','.join(map(str, plant))
                plant_code_condition = plant_code_condition + f""" and plant_code_observed in ('{plant}') """
            # else:
            #     plant_code_condition = f""" plant_code_observed in {tuple(plant_code)} """

            query = f""" select sr_no, short_explanation, long_explanation, observation_date, target_resolution_date, resolution_date,
                         quality_faillure_type, plant_code_observed, manufacturing_line_code_observed, functional_area, root_cause
                         from {app_constants.DatabaseTables.nc_processed_long} where 
                         cluster_name = '{cluster_name}' and severity = '{severity}' {res_status_condition} {plant_code_condition} {year_condition}
                         ORDER BY target_due_date DESC, sr_no limit {limit}  OFFSET  {offset * limit - limit}"""
            
            cluster_row_data = database_object.execute_select_query(query)
            cluster_data = []
            number_of_clusters = 0
            # if page_number is not None and size is not None:
            #     limit_condition = f" limit {size} offset {page_number * size - size} "
            # else:
            #     limit_condition = " "
            for each_cluster in cluster_row_data:
                number_of_clusters = cluster_row_data.index(each_cluster) + 1
                cluster_data.append({
                    "sr_no": each_cluster[0],
                    "short_explanation": each_cluster[1],
                    "long_explanation": each_cluster[2],
                    "observation_date": each_cluster[3],
                    "target_resolution_date": each_cluster[4],
                    "resolution_date": each_cluster[5],
                    "quality_faillure_type": each_cluster[6],
                    "plant_code_observed": each_cluster[7],
                    "manufacturing_line_code_observed": each_cluster[8],
                    "functional_area": each_cluster[9],
                    "root_cause": each_cluster[10]

                })
            result = {
                "number_of_clusters": number_of_clusters,
                "cluster_data":cluster_data
            }
            return app_constants.result_template(result)

        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while fetching cluster row data" + str(e))
            raise HTTPException(status_code=500, detail=str(e))


    def download_row_data(self):
        try:
            """
            Function to download entire cluster data in a file
            :return cluster details
            """
           
           # total_deviation_query = f""" SELECT sr_no, short_explanation, long_explanation, observation_date, target_resolution_date, resolution_date, quality_failure_type, plant_code_observed, manufacturing_line_code_observed, functional_area, root_cause  FROM {app_constants.DatabaseTables.nc_processed_long} """
            total_deviation_query = f""" select * from {app_constants.DatabaseTables.nc_processed_long} """
            total_deviation_data = database_object.execute_select_query(total_deviation_query)
           
            total_deviation_list_data = pd.DataFrame(total_deviation_data)
            
            total_deviation_list_data.to_csv('reports/Deviationdata.csv')
            
            upload_total_dev_file = s3_object.upload_file_to_s3(str(app_configurations.file_path)+ "/Deviationdata.csv", "deviation-analysis-row-data/DeviationData.csv")
            print(app_configurations.s3_bucket_path)
            #k = os.path.join(app_configurations.file_path, )
            
            
           # return app_constants.result_template(response)
        except Exception as e:
            traceback.print_exc()
            # logger.exception("Error occured while downloading data" + str(e))
            raise HTTPException(status_code=500, detail=str(e))

    def get_refresh_date(self):
        try:
            """
            Function to show latest refresh date
            : return date
            """
            import datetime
            datetime_object = datetime.date.today()
            result = {"refresh_date":datetime_object}
            return app_constants.result_template(result)
        except Exception as e:
            traceback.print_exc()
            raise HTTPException(status_code = 500, detail = str(e))
