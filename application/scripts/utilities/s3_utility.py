import boto3
import botocore
from fastapi import HTTPException
from application.scripts.config  import app_configurations
import traceback

class S3Utility(object):
    def __init__(self):
        
        self.conn_s3 = boto3.client('s3',region_name = app_configurations.region_name)

    def upload_file_to_s3(self, file_path, relative_s3_path):
        try:
            self.conn_s3.upload_file(file_path, app_configurations.s3_bucket_name, relative_s3_path)
        except Exception as e:
            traceback.print_exc()
            raise HTTPException(status_code= 500, detail = str(e))

    def presign_s3(self, file_path, expiration = 3600):
        """
        Function to generate pre signed url on S3 (Default 1 hour expiration)
        :param file path
        :param expiration
        """
        try:
            params = {
                    'Bucket':app_configurations.s3_bucket_name,
                    'Key':file_path}
            signed_url = self.conn_s3.generate_presigned_url('get_object', Params = params, ExpiresIn = expiration)
            return signed_url

        except Exception as e:
            #log.info("Error while generating the pre signed url %s, str(e))
            raise HTTPException(status_code = 500, detail = str(e))
