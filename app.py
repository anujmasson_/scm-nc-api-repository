from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
import traceback
import os
from application.scripts.core.services import deviation_analysis_services
from application.scripts.core.services import executive_summary_service
from application.scripts.config import app_configurations 
import sys

app = FastAPI(docs_url = "/api/nc/docs", redoc_url = None)
app.include_router(deviation_analysis_services.router)
app.include_router(executive_summary_service.router)

origins = ["*"]
app.add_middleware(CORSMiddleware, allow_origins = origins, allow_credentials = True, allow_methods=["*"], allow_headers=["*"])

@app.get("/")
def root():
    return "Supply Chain Management Project"

@app.get("/api/nc/")
def read_root():
    return "welcome to the  application"


# uvicorn --port=5000 --reload application:application
if __name__ == '__main__':
    try:
        uvicorn.run(app, host=str(app_configurations.server_host), port=int(app_configurations.server_port))
    except:
        traceback.print_exc()
